# -*- coding: utf-8 -*-
# © 2016 Yannick Vaucher (Camptocamp SA)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
from openerp import fields, models, models


class ResCompanyA(models.Model):
    _inherit = 'res.company'

    prefix_a_name = fields.Char()
    prefix_a_integer = fields.Integer()
    prefix_a_partner_id = fields.Many2one(comodel_name='res.partner')


class ResCompanyB(models.Model):
    _inherit = 'res.company'


    prefix_b_name = fields.Char('name')
    prefix_b_integer = fields.Integer('int')
    prefix_b_partner_id = fields.Many2one('res.partner')



class MyConfigA(models.TransientModel):
    _name = 'a.config.settings'
    _inherit = ['res.config.settings', 'abstract.config.settings']
    _prefix = 'prefix_a_'
    _companyObject = ResCompanyA


class MyConfigB(models.TransientModel):
    _name = 'b.config.settings'
    _inherit = ['res.config.settings', 'abstract.config.settings']
    _prefix = 'prefix_b_'
    _companyObject = ResCompanyB
