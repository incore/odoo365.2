`1.0.1`
-------

- **Fix:** shipping countries and states now comply with current website's delivery carriers countries and states. There is special user and group to get read access to delivery carriers from websites

`1.0.0`
-------

- **Init version**
