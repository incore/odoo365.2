=======================================
 Configurable SEO URL (Technical core)
=======================================

Allows to make custom SEO URL for pages related to some record.


E.g.

    /shop/product/configurable-seo-url-for-the-best-product

instead of

    /shop/product/name-of-product-123


See ``website_seo_url_product`` module as an example of usage.

Credits
=======

Contributors
------------
* Ivan Yelizariev <yelizariev@it-projects.info>

Sponsors
--------
* `IT-Projects LLC <https://it-projects.info>`__

Further information
===================

Demo: http://runbot.it-projects.info/demo/website-addons/10.0

HTML Description: https://apps.odoo.com/apps/modules/10.0/website_seo_url/

Usage instructions: `<doc/index.rst>`__

Changelog: `<doc/changelog.rst>`__

Tested on Odoo 10.0 47587de19b616120e14b1f40c8ff5b846a132364
