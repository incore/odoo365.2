# -*- coding: utf-8 -*-
{
    'name': "Product price factor for web shop",
    'version': '10.0.1.0.0',
    'summary': """Multiplies price depending on product attributes""",
    'category': 'Website',
    'license': 'GPL-3',
    'author': "IT-Projects LLC, Ildar Nasyrov",
    'price': 20.00,
    'currency': 'EUR',
    'images': ['images/1.png'],
    "support": "apps@it-projects.info",
    'website': "https://twitter.com/nasyrov_ildar",
    'depends': ['website_sale', 'product_price_factor'],
    'data': [
        'views/templates.xml',
    ],
    'auto_install': True,
}
