`1.2.0`
-------

- **Add:** Users with ``Current Backend Website`` specified have access only to sale orders from that website
- **Fix:** When use is logged in on different websites, he can use shopping carts on each websites independently 
- **Improvement:** group sale orders by website

`1.1.0`
-------

- **ADD:** you can now specify allowed websites for your payment acquirers

`1.0.2`
-------

- **ADD:** you can now specify allowed websites for your products

`1.0.1`
-------

- **FIX:** add multi-company support for carts

`1.0.0`
-------

- Init version
