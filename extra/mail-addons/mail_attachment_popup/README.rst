===================
 Popup Attachments
===================

 The module opens attached mail images in popup.

Credits
=======

Contributors
------------
* Dinar Gabbasov <gabbasov@it-projects.info>

Sponsors
--------
* `IT-Projects LLC <https://it-projects.info>`__

Maintainers
-----------
* `IT-Projects LLC <https://it-projects.info>`__

  The module is not maintained in future versions because it's functionality built-in since Odoo 11.0.

Further information
===================

Demo: http://runbot.it-projects.info/demo/mail-addons/10.0

HTML Description: https://apps.odoo.com/apps/modules/10.0/mail_attachment_popup/

Usage instructions: `<doc/index.rst>`_

Changelog: `<doc/changelog.rst>`_

Tested on Odoo 10.0 9cf666288076dc4e315e62e7ca0d6fc59995a498
