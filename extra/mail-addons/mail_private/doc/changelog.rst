`1.1.0`
-------

- **New**: added ability to select channels for private message sending.
- **New**: internal users are flagged automatically.

`1.0.1`
-------

- **PORT:** Odoo 10 support.

`1.0.0`
-------

- Init version
