# -*- coding: utf-8 -*-
# Copyright 2016 x620 <https://github.com/x620>
# Copyright 2018 Ruslan Ronzhin <https://it-projects.info/team/rusllan/>
# Copyright 2016-2019 Ivan Yelizariev <https://it-projects.info/team/yelizariev>
# License LGPL-3.0 (https://www.gnu.org/licenses/lgpl.html)
{
    "name": "Mail Base",
    "summary": """Makes Mail extendable""",
    "category": "Discuss",
    "images": [],
    "version": "10.0.1.1.0",

    "author": "IT-Projects LLC, Pavel Romanchenko",
    "support": "apps@it-projects.info",
    "website": "https://it-projects.info",
    "license": "LGPL-3",
    'price': 9.00,
    'currency': 'EUR',

    "depends": [
        "base",
        "mail"
    ],

    "data": [
        "views/templates.xml",
    ],
    'installable': True,
}
