`1.1.0`
-------

- **New**: channels are displayed in recipients


`1.0.1`
-------

- **FIX**: the problem with duplicating the names of recipients was solved.

`1.0.0`
-------

- Init version
