sudo su
sudo apt-get update && sudo apt-get upgrade -y && adduser --system --home=/opt/odoo --group odoo
apt-get install wkhtmltopdf ccze postgresql postgresql-server-dev-10 python-pip python-ldap libxml2-dev libxslt-dev node-less libsasl2-dev libldap2-dev aptitude -y
su - postgres
createuser --createdb --username postgres --no-createrole --no-superuser --pwprompt odoo
exit
cd /opt/odoo/ && git clone https://gitlab.com/incore/odoo365.2.git && mv odoo365.2 server && cd server
pip install num2words && pip install PyPDF2 && pip install pydrive && pip install simplejson && pip install oauthlib && pip install pysftp && pip install -r requirements.txt
mkdir /var/log/odoo/ && chown odoo:root /var/log/odoo && mkdir /etc/odoo && cp /opt/odoo/server/odoo-server.conf /etc/odoo/odoo.conf && chown odoo: /etc/odoo/odoo.conf && chmod 640 /etc/odoo/odoo.conf && chown -R odoo: /opt/odoo && chmod -R 775 /opt/odoo && cp /opt/odoo/server/init /etc/init.d/odoo && chmod 755 /etc/init.d/odoo && chown root: /etc/init.d/odoo && cd wkhtmltopdf && cp wkhtmltopdf /usr/local/bin && cp wkhtmltopdf /usr/bin/ && cp wkhtmltoimage /usr/local/bin && cp wkhtmltoimage /usr/bin/ && cd /opt/odoo/server
update-rc.d odoo defaults && /etc/init.d/odoo start && apt-get update && ln -s /var/log/odoo/odoo-server.log
tail -f  log | ccze


# -------------- install nginx ---------------------
apt-get install nginx software-properties-common -y
add-apt-repository ppa:certbot/certbot
apt-get update && apt-get install certbot -y
mkdir /etc/nginx/ssl && openssl dhparam -out /etc/nginx/ssl/dhp-2048.pem 2048

# para subdominios
service nginx stop && certbot certonly --standalone -d c1.pos365.info && service nginx start

# SSL WILDCARD
git clone https://github.com/certbot/certbot.git && cd certbot && ./certbot-auto
./certbot-auto certonly --manual --preferred-challenges=dns --email hola@incore.co --server https://acme-v02.api.letsencrypt.org/directory --agree-tos -d *.pos365.info

# Dominios
service nginx stop && certbot certonly --standalone -d c1.pos365.info -d www.c1.pos365.info && service nginx start

#crear zona
nano /etc/nginx/sites-available/c1.pos365.info

# contenido del archivo
upstream c1.pos365.info {
    server 127.0.0.1:8080;
}

server {
    listen      443;
    server_name c1.pos365.info www.c1.pos365.info;

    access_log  /var/log/nginx/c1.pos365.info.access.log;
    error_log   /var/log/nginx/c1.pos365.info.error.log;

    ssl on;
    ssl_certificate     /etc/letsencrypt/live/c1.pos365.info/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/c1.pos365.info/privkey.pem;
    keepalive_timeout   60;

    ssl_ciphers "ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:ECDH+3DES:DH+3DES:RSA+AESGCM:RSA+AES:RSA+3DES:!aNULL:!MD5:!DSS:!AES256";
    ssl_protocols           TLSv1 TLSv1.1 TLSv1.2;
    ssl_prefer_server_ciphers on;
    ssl_dhparam /etc/nginx/ssl/dhp-2048.pem;

    proxy_buffers 16 64k;
    proxy_buffer_size 128k;

    location / {
        proxy_pass  http://c1.pos365.info;
        proxy_next_upstream error timeout invalid_header http_500 http_502 http_503 http_504;
        proxy_redirect off;

        proxy_set_header    Host            $host;
        proxy_set_header    X-Real-IP       $remote_addr;
        proxy_set_header    X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header    X-Forwarded-Proto https;
    }

    location ~* /web/static/ {
        proxy_cache_valid 200 60m;
        proxy_buffering on;
        expires 864000;
        proxy_pass http://c1.pos365.info;
    }
}

server {
    listen      80;
    server_name c1.pos365.info www.c1.pos365.info;

    add_header Strict-Transport-Security max-age=2592000;
    rewrite ^/.*$ https://$host$request_uri? permanent;
}
 # fin del contenido del archivo



#habilitar zona
ln -s /etc/nginx/sites-available/c1.pos365.info /etc/nginx/sites-enabled/c1.pos365.info && /etc/init.d/nginx restart


# ----------------install saas ---------------------
sudo pip install boto rotate_backups_s3 pysftp oauthlib simplejson
